README
=====

This repository contains code, data, and output from an effort to visualize the themes in session titles for all past SOLE meetings. 

The code behind this endeavor is available at [https://bitbucket.org/vilhuberl/sole2017](https://bitbucket.org/vilhuberl/sole2017), and you can find the browsable page at [http://www.vrdc.cornell.edu/sole2017/](http://www.vrdc.cornell.edu/sole2017/).

[Lars Vilhuber](mailto:lars.vilhuber@cornell.edu), [Cornell University](http://www.cornell.edu) [Economics Department](http://economics.cornell.edu) and [Labor Dynamics Institute](http://www.ilr.cornell.edu/ldi/).

