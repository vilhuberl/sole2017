# SOLE 2015 Posters as a picture
Lars Vilhuber  

======================


```
## Loading required package: RColorBrewer
```

```
## Loading required package: NLP
```

Or: the power of **evidence**.

A picture is worth a thousand words. Or in this case 488 words - the number of distinct words in titles of the 101 papers accepted for a poster session at [SOLE 2015](http://www.sole-jole.org/2015.htm). 

![](graphing.SOLE2017.posters_only_files/figure-html/graph-1.png)<!-- -->

To produce this graph, once all papers had been allocated to either poster or full paper status (as per the [program as of May 19, 2015](http://www.sole-jole.org/2015FULLPROGRAM.html)), we  read in [the data](PosterTitles.csv) (provided by the organizing committee in a convenient format) using R:

```r
# Source: titles of all accepted papers (posters and sessions)
accepted <- read.csv("data/PosterTitles.tsv",sep="\t",header=FALSE,col.names=c("Year","Title"))
# accepted may have been a subset
```
We then used the _R text mining library_  to clean and parse the titles:

```r
year <- 2015
doc.vec <- VectorSource(t(subset(accepted,year==year)))
doc.corpus <- Corpus(doc.vec)
doc.corpus <- tm_map(doc.corpus, content_transformer(tolower))
doc.corpus.nw <- tm_map(doc.corpus, stripWhitespace)
doc.corpus <- tm_map(doc.corpus, removePunctuation)
doc.corpus <- tm_map(doc.corpus, removeNumbers) 
doc.corpus <- tm_map(doc.corpus, removeWords, stopwords("english"))
rmthe <- function (x)
gsub("The","",x)
doc.corpus <- tm_map(doc.corpus, content_transformer(rmthe))

TDM <- TermDocumentMatrix(doc.corpus)
try_max <- 20
try_five <- 10
restrict_num <- 3
most_freq <- findFreqTerms(TDM,try_max)
```
which generated a "corpus" of documents.


In fact, we lied somewhat above: we did not show **488** words, but rather, for the sake of clarity, restricted ourselves to the **53** words with at least 3 mentions in the (cleaned) corpus. If we had not, we would have obtained  the [following graph](graphing.SOLE2015.all_titles.png):

![](graphing.SOLE2017.posters_only_files/figure-html/graph_all_titles-1.png)<!-- -->


For the curious, while the most frequent word is **evidence**, the top **4** are:

```
##   findFreqTerms(TDM, try_five)
## 1                     evidence
## 2                        labor
## 3                      effects
## 4                         wage
```

Because there is a fair bit of randomness in the graph generation, we ran through the last R chunk a few times, generating a few alternate, high-resolution versions:

 * [Attempt 1](graphing.SOLE2015.all_titles.HIRES1.png)
 * [Attempt 2](graphing.SOLE2015.all_titles.HIRES2.png)
 * [Attempt 3](graphing.SOLE2015.all_titles.HIRES3.png)


*******************
 * The code behind this endeavor is available at [https://bitbucket.org/vilhuberl/sole2017](https://bitbucket.org/vilhuberl/sole2017).
 * This document was produced using

```r
R.Version()
```

```
## $platform
## [1] "x86_64-suse-linux-gnu"
## 
## $arch
## [1] "x86_64"
## 
## $os
## [1] "linux-gnu"
## 
## $system
## [1] "x86_64, linux-gnu"
## 
## $status
## [1] ""
## 
## $major
## [1] "3"
## 
## $minor
## [1] "3.3"
## 
## $year
## [1] "2017"
## 
## $month
## [1] "03"
## 
## $day
## [1] "06"
## 
## $`svn rev`
## [1] "72310"
## 
## $language
## [1] "R"
## 
## $version.string
## [1] "R version 3.3.3 (2017-03-06)"
## 
## $nickname
## [1] "Another Canoe"
```

```r
Sys.info()
```

```
##                                                 sysname 
##                                                 "Linux" 
##                                                 release 
##                                     "3.16.7-29-desktop" 
##                                                 version 
## "#1 SMP PREEMPT Fri Oct 23 00:46:04 UTC 2015 (6be6a97)" 
##                                                nodename 
##                                              "zotique2" 
##                                                 machine 
##                                                "x86_64" 
##                                                   login 
##                                              "vilhuber" 
##                                                    user 
##                                              "vilhuber" 
##                                          effective_user 
##                                              "vilhuber"
```
