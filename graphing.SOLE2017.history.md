# SOLE Session Titles as a picture
Lars Vilhuber  

======================



Or: the power of **labor**.

A picture is worth a thousand words. Or in this case 265 words - the number of distinct words in titles of the 234 sessions at SOLE meetings between 1996 and 2016


![](graphing.SOLE2017.history_files/figure-html/graph-1.png)<!-- -->

To produce this graph, a bunch of RAs classified all titles into JEL codes.  We  read in [the data](data/SOLE_JEL_code_data.csv) (provided by the organizing committee in a convenient format) using R:

```r
# Source: titles of all accepted papers (posters and sessions)
#accepted <- read.csv("data/PosterTitles.tsv",sep="\t",header=FALSE,col.names=c("Year","Title"))
# accepted may have been a subset
accepted <- read.csv("data/SOLE - JEL code data.csv")
papers.raw <- read_excel("data/Paper 96-17(no 990203).xls")
# This was manually defined by inspection of the file. These are the years with "good" data
keep.papers <- c("1996","1997","1998","2001","2004","2005","2006","2007","2009", "2010","2011","2013","2016")
papers <- subset(papers.raw,Year %in% keep.papers)
choice.years <- c("1996","2006","2016")

start.year <- min(accepted$Year)
end.year <- max(accepted$Year)
point.year <- 2017

# one time action: we get the JEL codes
#jel.url <- "https://www.aeaweb.org/econlit/classifications.xml"
jel.doc <- xmlParse("data/classifications.xml")
jel.codes <- xmlToDataFrame(jel.doc)
names(jel.codes)[1] <- "JEL"
```
We then used the _R text mining library_  to clean and parse the titles:

```r
#doc.vec <- VectorSource(t(subset(accepted,year==year)))
parse_data <- function(tdata,remove_numbers = FALSE) {
doc.vec <- VectorSource(t(tdata))
doc.corpus <- Corpus(doc.vec)
doc.corpus <- tm_map(doc.corpus, content_transformer(tolower))
doc.corpus.nw <- tm_map(doc.corpus, stripWhitespace)
doc.corpus <- tm_map(doc.corpus, removePunctuation)
if(remove_numbers) {doc.corpus <- tm_map(doc.corpus, removeNumbers) }
doc.corpus <- tm_map(doc.corpus, removeWords, stopwords("english"))
rmthe <- function (x)
gsub("The","",x)
doc.corpus <- tm_map(doc.corpus, content_transformer(rmthe))

TDM <- TermDocumentMatrix(doc.corpus)
return(TDM)
}
# now call the function
TDM <- parse_data(accepted$SessionTitle)
try_max <- max(rowSums(as.matrix(TDM)))
try_five <- 10
restrict_num <- 3
most_freq <- findFreqTerms(TDM,try_max)
```
which generated a "corpus" of documents.


In fact, we lied somewhat above: we did not show **265** words, but rather, for the sake of clarity, restricted ourselves to the **66** words with at least 3 mentions in the (cleaned) corpus. If we had not, we would have obtained  the [following graph](graphing.SOLE2017.all_titles.png):

![](graphing.SOLE2017.history_files/figure-html/graph_all_titles-1.png)<!-- -->


For the curious, while the most frequent word is **labor**, the top **10** are:

```
##     labor    market education economics  outcomes   markets    health 
##        40        18        16        16        14        13        11 
##  workshop    gender     wages 
##        11        10        10
```

## A few variations
Let's try it with JEL codes.
![](graphing.SOLE2017.history_files/figure-html/graph_JEL-1.png)<!-- -->

As it turns out, the 10 top JEL codes are

```r
head(jel.freqs,try_five)
```

```
##    JEL freq
## 53 J24   40
## 32 I21   30
## 45 J13   29
## 56 J31   27
## 70 J64   19
## 68 J62   17
## 90 M52   17
## 67 J61   15
## 73 J71   15
## 37 I26   14
##                                                                     description
## 53               Human Capital; Skills; Occupational Choice; Labor Productivity
## 32                                                        Analysis of Education
## 45                      Fertility; Family Planning; Child Care; Children; Youth
## 56                                 Wage Level and Structure; Wage Differentials
## 70                    Unemployment: Models, Duration, Incidence, and Job Search
## 68                 Job, Occupational, and Intergenerational Mobility; Promotion
## 90 Personnel Economics: Compensation and Compensation Methods and Their Effects
## 67                                 Geographic Labor Mobility; Immigrant Workers
## 73                                                         Labor Discrimination
## 37                                                         Returns to Education
```

## Historical data
Let's have a look at past years: 1996, 2006, 2016. In all cases, we only use words showing up with at least 3 mentions. Note that it is not normalized by the total number of sessions (increasing from 31 papers in `min(choice.years)` to 196 papers in 2016).

![](graphing.SOLE2017.history_files/figure-html/graph_years-1.png)<!-- -->![](graphing.SOLE2017.history_files/figure-html/graph_years-2.png)<!-- -->![](graphing.SOLE2017.history_files/figure-html/graph_years-3.png)<!-- -->




Of course, if we wanted to show the plot with the same 8 top terms, this is what it would look like (after removing the year):

![](graphing.SOLE2017.history_files/figure-html/graph_years_same-1.png)<!-- -->![](graphing.SOLE2017.history_files/figure-html/graph_years_same-2.png)<!-- -->![](graphing.SOLE2017.history_files/figure-html/graph_years_same-3.png)<!-- -->


*******************
 * The code behind this endeavor is available at [https://bitbucket.org/vilhuberl/sole2017](https://bitbucket.org/vilhuberl/sole2017).
 * This document was produced using

```r
R.Version()
```

```
## $platform
## [1] "x86_64-suse-linux-gnu"
## 
## $arch
## [1] "x86_64"
## 
## $os
## [1] "linux-gnu"
## 
## $system
## [1] "x86_64, linux-gnu"
## 
## $status
## [1] ""
## 
## $major
## [1] "3"
## 
## $minor
## [1] "4.0"
## 
## $year
## [1] "2017"
## 
## $month
## [1] "04"
## 
## $day
## [1] "21"
## 
## $`svn rev`
## [1] "72570"
## 
## $language
## [1] "R"
## 
## $version.string
## [1] "R version 3.4.0 (2017-04-21)"
## 
## $nickname
## [1] "You Stupid Darkness"
```

```r
Sys.info()
```

```
##                                                sysname 
##                                                "Linux" 
##                                                release 
##                             "4.8.6-7.g1efad3c-default" 
##                                                version 
## "#1 SMP PREEMPT Tue Nov 8 15:14:30 UTC 2016 (1efad3c)" 
##                                               nodename 
##                     "iveshallX1.cloutier-vilhuber.net" 
##                                                machine 
##                                               "x86_64" 
##                                                  login 
##                                             "vilhuber" 
##                                                   user 
##                                             "vilhuber" 
##                                         effective_user 
##                                             "vilhuber"
```
